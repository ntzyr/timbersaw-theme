<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-03-22
 * Time: 00:01
 */

namespace TimberSaw\Assets;

use TimberSaw\Core\Assets;

class Front extends Assets
{
    public function frontAssets()
    {
        wp_enqueue_style('timbersaw/public/css/app', $this->public . 'css/app.css');
        wp_enqueue_script('timbersaw/public/js/app', $this->public . 'js/app.js');
    }
}