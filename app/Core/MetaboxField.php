<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-04-02
 * Time: 08:47
 */

namespace TimberSaw\Core;


use Timber\Timber;

class MetaboxField
{
    public $metabox_id;
    public $field;
    
    public function __construct(string $metabox_id, array $field = null)
    {
        if($field == null ) return new \Exception(__('Empty field data','timbersaw-theme'));

        $this->metabox_id = $metabox_id;
        $this->field = $field;

        if($field['type'] == null || !isset($field['type'])) return new \Exception(__('Empty field data','timbersaw-theme'));

        $this->render();
    }

    public function render()
    {
        global $post;

        $metabox_id = $this->metabox_id;
        $field = $this->field;
        $templateName = $field['type'];

        $value = get_post_meta($post->ID, $metabox_id, true);

        $data = array(
            'metabox_id' => $metabox_id,
            'field' => $field,
            'value' => isset($value[$field['id']]) && $value[$field['id']] != "" ? $value[$field['id']] : null
        );

        Timber::render("/metabox/$templateName.twig", $data);
    }
}