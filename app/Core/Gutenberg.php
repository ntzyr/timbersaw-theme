<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-03-29
 * Time: 00:46
 */

namespace TimberSaw\Core;


class Gutenberg
{
    public $root;
    public $name;

    public function __construct($root, $name)
    {
        $this->root = $root;
        $this->name = $name;

        add_action('init', array($this, 'block'));
    }

    public function block()
    {
        $root = $this->root;
        $name = $this->name;

        wp_register_script(
            $root . '/' . $name . '-editor',
            get_template_directory_url() . '/static/js/gutenberg/' . $root . '/' . $name . '-editor.js',
            array( 'wp-blocks', 'wp-i18n', 'wp-element' )
        );

        wp_register_style(
            $root . '/' . $name . '-editor',
            get_template_directory_url() . '/static/js/gutenberg/' . $root . '/' . $name . '-editor.css'
        );

        wp_register_style(
            $root . '/' . $name,
            get_template_directory_url() . '/static/js/gutenberg/' . $root . '/' . $name . '.css'
        );
        
        register_block_type($root . '/' . $name, array(
            'style' => $root . '/' . $name,
            'editor_style' => $root . '/' . $name . '-editor',
            'editor_script' => $root . '/' . $name . '-editor'
        ));
    }
}