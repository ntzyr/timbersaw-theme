<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-03-29
 * Time: 14:17
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
Timber::render( array( '/front/page-' . $post->post_name . '.twig', '/front/page.twig' ), $context );
