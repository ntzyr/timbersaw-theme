<?php
/**
 * Created by PhpStorm.
 * User: ntzyr
 * Date: 2019-03-29
 * Time: 14:18
 */


$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;

if ( post_password_required( $post->ID ) ) {
    Timber::render( '/front/single-password.twig', $context );
} else {
    Timber::render( array( '/front/single-' . $post->ID . '.twig', '/front/single-' . $post->post_type . '.twig', '/front/single.twig' ), $context );
}
